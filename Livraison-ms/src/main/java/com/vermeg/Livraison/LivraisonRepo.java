package com.vermeg.Livraison;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LivraisonRepo extends JpaRepository<Livraison,Integer>  {
}
