package com.vermeg.Livraison;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ServiceLivraison  implements  IService{
    LivraisonRepo lvrRp ;
    @Override
    public Livraison addlvr(Livraison lvr) {
        return lvrRp.save(lvr);
    }

    @Override
    public List<Livraison> retrieveall() {
        return lvrRp.findAll();
    }
}
