package com.vermeg.Livraison;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/livraison")
@AllArgsConstructor
public class LivraisonCtrl {
    LivraisonRepo lvrR ;
    ServiceLivraison ServiceLvr ;
    @PostMapping("/add")
    public Livraison add(@RequestBody Livraison pr){
        return ServiceLvr.addlvr(pr);
    }
    @GetMapping("/getAll")
    public List<Livraison> GetAll(){
        return ServiceLvr.retrieveall();

    }
}
